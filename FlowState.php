<?php

namespace JB\FlowRiddleSolver;

class FlowState
{
	private $data;
	private $width;
	private $height;
	private $endPoints;

	public function __construct($Width, $Height)
	{
		$this->width = $Width;
		$this->height = $Height;
		$this->data = array();
		$this->endPoints = array();

		for ($x = 0; $x < $this->width; $x++)
			for ($y = 0; $y < $this->height; $y++)
				$this->Set($x, $y, 0);
	}

	public function GetWidth()
	{
		return $this->width;
	}

	public function GetHeight()
	{
		return $this->height;
	}

	private function positionToIndex($x, $y)
	{
		if ($this->IsValid($x, $y))
		{
			return $x + $this->width * $y;
		}

		throw new \Exception("Index out of bounds");
	}

	public function IsValid($x, $y)
	{
		return 0 <= $x && $x < $this->width &&
					 0 <= $y && $y < $this->height;
	}

	public function Set($x, $y, $value)
	{
		$this->data[$this->positionToIndex($x, $y)] = $value;
		if (0 < $value && array_key_exists($value, $this->endPoints))
		{
			if (!array_key_exists('x1', $this->endPoints[$value]))
			{
				$this->endPoints[$value]['x1'] = $x;
				$this->endPoints[$value]['y1'] = $y;
			}
			elseif (!array_key_exists('x2', $this->endPoints[$value]))
			{
				$this->endPoints[$value]['x2'] = $x;
				$this->endPoints[$value]['y2'] = $y;
			}
		}
		else
		{
			$this->endPoints[$value] = array('x1' => $x, 'y1' => $y);
		}
	}

	public function Get($x, $y)
	{
		return $this->data[$this->positionToIndex($x, $y)];
	}

	public function GetEndpoint($color, $index)
	{
		if (array_key_exists($color, $this->endPoints))
		{
			if (array_key_exists("x$index", $this->endPoints[$color]))
			{
				return array($this->endPoints[$color]["x$index"], $this->endPoints[$color]["y$index"]);
			}

			throw new \Exception("Point of given color not indexed");
		}

		throw new \Exception("Color not indexed");
	}

	public function MoveEndpoint($color, $index, $offsetX, $offsetY)
	{
		if (array_key_exists($color, $this->endPoints))
		{
			if (array_key_exists("x$index", $this->endPoints[$color]))
			{
				$this->endPoints[$color]["x$index"] += $offsetX;
				$this->endPoints[$color]["y$index"] += $offsetY;
				return;
			}

			throw new \Exception("Point of given color not indexed");
		}

		throw new \Exception("Color not indexed");
	}

	public function GetColors()
	{
		return array_keys($this->endPoints);
	}
};
