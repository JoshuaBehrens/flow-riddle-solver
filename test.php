<?php

require_once(dirname(__FILE__)."/Solver.php");

error_reporting(-1);
ini_set('display_errors', 'On');

function printState($state)
{
	for ($y = 0; $y < $state->GetHeight(); $y++)
	{
		for ($x = 0; $x < $state->GetWidth(); $x++)
		{
			echo $state->Get($x, $y) > 0 ? $state->Get($x, $y) : ' ';
		}
		echo PHP_EOL;
	}
	echo "-----".PHP_EOL;
}

$state = new \JB\FlowRiddleSolver\FlowState(5, 5);

$state->Set(0, 0, 1);
$state->Set(1, 4, 1);

$state->Set(2, 0, 2);
$state->Set(1, 3, 2);

$state->Set(4, 0, 3);
$state->Set(3, 3, 3);

$state->Set(2, 1, 4);
$state->Set(2, 4, 4);

$state->Set(4, 1, 5);
$state->Set(3, 4, 5);

printState($state);
$state = \JB\FlowRiddleSolver\Solver::Solve($state);
if (!is_null($state))
	printState($state);
