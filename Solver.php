<?php

namespace JB\FlowRiddleSolver;

require_once(dirname(__FILE__)."/FlowState.php");

class Solver
{
	private static function isColorSolved(FlowState $state, $color)
	{
		list($x1, $y1) = $state->GetEndpoint($color, 1);
		list($x2, $y2) = $state->GetEndpoint($color, 2);

		return $x1 == $x2 && $y1 == $y2;
	}

	private static function nextCornerStepHelp(FlowState $state, $x, $y, $offX, $offY, $color)
	{
		if ($state->Get($x, $y) == $color)
		{
			if ($state->IsValid($x + $offX, $y + $offY) && $state->Get($x + $offX, $y + $offY) == 0)
			{
				$open = 0;

				if ($state->IsValid($x + $offX + 1, $y + $offY) && $state->Get($x + $offX + 1, $y + $offY) == 0) $open++;
				if ($state->IsValid($x + $offX - 1, $y + $offY) && $state->Get($x + $offX - 1, $y + $offY) == 0) $open++;
				if ($state->IsValid($x + $offX, $y + $offY + 1) && $state->Get($x + $offX, $y + $offY + 1) == 0) $open++;
				if ($state->IsValid($x + $offX, $y + $offY - 1) && $state->Get($x + $offX, $y + $offY - 1) == 0) $open++;

				return $open;
			}
		}

		return -1;
	}

	private static function cornerFiller(FlowState $state, $color = 0)
	{
		if ($color < 1)
		{
			foreach ($state->GetColors() as $clr)
			{
				if ($clr == 0 || self::isColorSolved($state, $clr))
					continue;

				$dummy = self::cornerFiller($state, $clr);

				if (!is_null($dummy))
				{
					return $dummy;
				}
			}
		}
		else
		{
			foreach (array(1,2) as $i)
			{
				list($pointX, $pointY) = $state->GetEndpoint($color, $i);
				if (self::nextCornerStepHelp($state, $pointX, $pointY, -1, 0, $color) == 1)
				{
					$result = clone $state;
					$result->Set($pointX - 1, $pointY, $color);
					$result->MoveEndpoint($color, $i, -1, 0);
					return $result;
				}

				if (self::nextCornerStepHelp($state, $pointX, $pointY, 1, 0, $color) == 1)
				{
					$result = clone $state;
					$result->Set($pointX + 1, $pointY, $color);
					$result->MoveEndpoint($color, $i, 1, 0);
					return $result;
				}

				if (self::nextCornerStepHelp($state, $pointX, $pointY, 0, -1, $color) == 1)
				{
					$result = clone $state;
					$result->Set($pointX, $pointY - 1, $color);
					$result->MoveEndpoint($color, $i, 0, -1);
					return $result;
				}

				if (self::nextCornerStepHelp($state, $pointX, $pointY, 0, 1, $color) == 1)
				{
					$result = clone $state;
					$result->Set($pointX, $pointY + 1, $color);
					$result->MoveEndpoint($color, $i, 0, 1);
					return $result;
				}
			}
		}

		return null;
	}

	private static function oneWays(FlowState $state)
	{
		foreach ($state->GetColors() as $clr)
		{
			if ($clr == 0 || self::isColorSolved($state, $clr))
				continue;

			foreach (array(1, 2) as $i)
			{
				list($x, $y) = $state->GetEndpoint($clr, 1);

				$open = 0;
				$nx = -1;
				$ny = -1;

				if ($state->IsValid($x + 1, $y) && $state->Get($x + 1, $y) == 0)
				{
					$nx = $x + 1;
					$ny = $y;
					$open++;
				}
				if ($state->IsValid($x - 1, $y) && $state->Get($x - 1, $y) == 0)
				{
					$nx = $x - 1;
					$ny = $y;
					$open++;
				}
				if ($state->IsValid($x, $y - 1) && $state->Get($x, $y - 1) == 0)
				{
					$nx = $x;
					$ny = $y - 1;
					$open++;
				}
				if ($state->IsValid($x, $y + 1) && $state->Get($x, $y + 1) == 0)
				{
					$nx = $x;
					$ny = $y + 1;
					$open++;
				}

				if ($open == 1)
				{
					$result = clone $state;
					$result->MoveEndpoint($clr, $i, $nx - $x, $ny - $y);
					$result->Set($nx, $ny, $clr);
					return $result;
				}
			}
		}

		return null;
	}

	private static function nextConnect(FlowState $state)
	{
		foreach ($state->GetColors() as $clr)
		{
			if ($clr == 0 || self::isColorSolved($state, $clr))
				continue;

			list($x1, $y1) = $state->GetEndpoint($clr, 1);
			list($x2, $y2) = $state->GetEndpoint($clr, 2);

			if ((abs($x2 - $x1) * abs($y2 - $y1)) == 0 &&
					(abs($x2 - $x1) + abs($y2 - $y1)) == 2)
			{
				$result = clone $state;
				$result->Set(($x2 + $x1) / 2, ($y2 + $y1) / 2, $clr);
				$result->MoveEndpoint($clr, 1, $x2 - $x1, $y2 - $y1);
				return $result;
			}
		}

		return null;
	}

	private static function nextStepHelper(FlowState $state)
	{
		if (!is_null($dummy = self::oneWays($state)))
		{
			$state = $dummy;

			if (!is_null($dummy = self::nextConnect($state)))
			{
				$state = $dummy;

				if (!is_null($dummy = self::cornerFiller($state)))
				{
					return $dummy;
				}

				return $state;
			}

				return $state;
		}

		return null;
	}

	public static function NextStep(FlowState $state)
	{
		$dummy = self::nextStepHelper($state);
		return is_null($dummy) ? $state : $dummy;
	}

	public static function Solve(FlowState $state)
	{
		while (!is_null($dummy = self::nextStepHelper($state)))
		{
			$state = $dummy;
		}

		return $state;
	}
};